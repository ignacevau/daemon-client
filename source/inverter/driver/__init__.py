# region type_checking
from enum import Enum
from typing import List
# endregion /type_checking

from abc import ABC, abstractmethod
from inverter import Inverter


class InverterData:
    def __init__(self,
        time:int,
        grid_voltage:str,
        grid_frequency:str,
        output_power:str,
        pv_input_voltage:str,
        pv_charging_power:str,
        battery_charging_current:str,
        battery_discharging_current:str,
        power_source:str,
        battery_voltage:str,
        load_percent:str):
        
        self._time = time
        self._grid_voltage = grid_voltage
        self._grid_frequency = grid_frequency
        self._output_power = output_power
        self._pv_input_voltage = pv_input_voltage
        self._pv_charging_power = pv_charging_power
        self._battery_charging_current = battery_charging_current
        self._battery_discharging_current = battery_discharging_current
        self._power_source = power_source
        self._battery_voltage = battery_voltage
        self._load_percent = load_percent

    def __str__(self) -> str:
        return """Inverter readonly data:
    time: %s
    grid_voltage: %s
    grid_frequency: %s
    output_power: %s
    pv_input_voltage: %s
    pv_charging_power: %s
    battery_charging_current: %s
    battery_discharging_current: %s
    power_source: %s
    battery_voltage: %s
    load_percent: %s"""%(
        self._time,
        self._grid_voltage,
        self._grid_frequency,
        self._output_power,
        self._pv_input_voltage,
        self._pv_charging_power,
        self._battery_charging_current,
        self._battery_discharging_current,
        self._power_source,
        self._battery_voltage,
        self._load_percent)


    def to_dict(self) -> dict:
        return {
            "time": self._time,
            "grid_voltage": self._grid_voltage,
            "grid_frequency": self._grid_frequency,
            "output_power": self._output_power,
            "pv_input_voltage": self._pv_input_voltage,
            "pv_charging_power": self._pv_charging_power,
            "battery_charging_current": self._battery_charging_current,
            "battery_discharging_current": self._battery_discharging_current,
            "power_source": self._power_source,
            "battery_voltage": self._battery_voltage,
            "load_percent": self._load_percent
        }

    
    def is_valid(self) -> bool:
        # catch Exceptions if None
        try:
            if self._time < 0:
                return False
            if float(self._grid_voltage) < 0:
                return False
            if float(self._grid_frequency) < 0:
                return False
            if int(self._output_power) < 0:
                return False
            if float(self._pv_input_voltage) < 0:
                return False
            if int(self._pv_charging_power) < 0:
                return False
            if int(self._battery_charging_current) < 0:
                return False
            if int(self._battery_discharging_current) < 0:
                return False
            if self._power_source not in ["P", "S", "L", "B", "F", "H"]:
                return False
            if float(self._battery_voltage) < 0:
                return False
            if int(self._load_percent) < 0:
                return False
        # couldn't parse data
        except:
            return False
        
        return True

    @property
    def time(self):
        return self._time

    @property
    def grid_voltage(self):
        return self._grid_voltage

    @property
    def grid_frequency(self):
        return self._grid_frequency

    @property
    def output_power(self):
        return self._output_power

    @property
    def pv_input_voltage(self):
        return self._pv_input_voltage
    
    @property
    def pv_charging_power(self):
        return self._pv_charging_power

    @property
    def battery_charging_current(self):
        return self._battery_charging_current

    @property
    def battery_discharging_current(self):
        return self._battery_discharging_current
    
    @property
    def power_source(self):
        return self._power_source


class InverterConfig:
    def __init__(self,
        time:int,
        output_source_priority:str,
        charger_source_priority:str,
        battery_recharge_voltage:str,
        battery_redischarge_voltage:str,
        max_charging_current:str,
        utility_max_charging_current:str,
        battery_bulk_voltage:str,
        battery_float_voltage:str,
        battery_cutoff_voltage:str,
        input_voltage_range:str):
        
        self._time = time
        self._output_source_priority = output_source_priority
        self._charger_source_priority = charger_source_priority
        self._battery_recharge_voltage = battery_recharge_voltage
        self._battery_redischarge_voltage = battery_redischarge_voltage
        self._max_charging_current = max_charging_current
        self._utility_max_charging_current = utility_max_charging_current
        self._battery_bulk_voltage = battery_bulk_voltage
        self._battery_float_voltage = battery_float_voltage
        self._battery_cutoff_voltage = battery_cutoff_voltage
        self._input_voltage_range = input_voltage_range

    def __str__(self) -> str:
        return """Inverter config data:
    time: %s
    output_source_priority: %s
    charger_source_priority: %s
    battery_recharge_voltage: %s
    battery_redischarge_voltage: %s
    max_charging_current: %s
    utility_max_charging_current: %s
    battery_bulk_voltage: %s
    battery_float_voltage: %s
    battery_cutoff_voltage: %s
    input_voltage_range: %s"""%(
                    self._time,
                    self._output_source_priority,
                    self._charger_source_priority,
                    self._battery_recharge_voltage,
                    self._battery_redischarge_voltage,
                    self._max_charging_current,
                    self._utility_max_charging_current,
                    self._battery_bulk_voltage,
                    self._battery_float_voltage,
                    self._battery_cutoff_voltage,
                    self._input_voltage_range)

    def is_valid(self) -> bool:
        # catch Exceptions if None
        try:
            if self._time < 0:
                return False
            if self.output_source_priority not in ["0", "1", "2"]:
                return False
            if self.charger_source_priority not in ["0", "1", "2", "3"]:
                return False
            if float(self.battery_recharge_voltage) < 0:
                return False
            if float(self.battery_redischarge_voltage) < 0:
                return False
            if int(self.max_charging_current) < 0:
                return False
            if int(self.utility_max_charging_current) < 0:
                return False
            if float(self.battery_bulk_voltage) < 0:
                return False
            if float(self.battery_float_voltage) < 0:
                return False
            if float(self.battery_cutoff_voltage) < 0:
                return False
            if self.input_voltage_range not in ["0", "1"]:
                return False
        # couldn't parse data
        except:
            return False
        
        return True

    @property
    def time(self):
        return self._time

    @property
    def output_source_priority(self):
        return self._output_source_priority
        
    @property
    def charger_source_priority(self):
        return self._charger_source_priority
        
    @property
    def battery_recharge_voltage(self):
        return self._battery_recharge_voltage
        
    @property
    def battery_redischarge_voltage(self):
        return self._battery_redischarge_voltage
        
    @property
    def max_charging_current(self):
        return self._max_charging_current
        
    @property
    def utility_max_charging_current(self):
        return self._utility_max_charging_current
        
    @property
    def battery_bulk_voltage(self):
        return self._battery_bulk_voltage
        
    @property
    def battery_float_voltage(self):
        return self._battery_float_voltage
        
    @property
    def battery_cutoff_voltage(self):
        return self._battery_cutoff_voltage
        
    @property
    def input_voltage_range(self):
        return self._input_voltage_range

            
class IDriver(ABC):
    @classmethod
    @abstractmethod
    def is_inverter_compatible(cls, vendor_id:str, product_id:str) -> bool:
        """Check whether a usb inverter is compatible with the driver
        by comparing the vendor-id and product-id

        Args:
            vendor_id (str): vendor-id
            product_id (str): product-id

        Returns:
            bool: True if compatible
        """
        pass


    @abstractmethod
    def load(self) -> None:
        """Load the driver to send and receive data to the inverter(s)
        """
        pass
    
    @abstractmethod
    def get_inverters(self) -> List[Inverter]:
        """Return a list of all the inverters connected to the device

        Returns:
            List[Inverter]: list of inverters
        """
        pass

    @abstractmethod
    def get_inverter_data(self, serial_number:str) -> InverterData:
        """Return all the non-configurable data from the inverter(s)

        Returns:
            InverterData: data
        """
        pass

    @abstractmethod
    def get_inverter_config(self, serial_number:str) -> InverterConfig:
        """Return all the configurable data from the inverter(s)

        Returns:
            InverterConfig: _description_
        """
        pass

    @abstractmethod
    def set_output_source(self, value):
        pass

    @abstractmethod
    def set_charger_source(self, value):
        pass

    @abstractmethod
    def set_battery_recharge_voltage(self, value):
        pass

    @abstractmethod
    def set_battery_redischarge_voltage(self, value):
        pass

    @abstractmethod
    def set_max_charging_current(self, value):
        pass

    @abstractmethod
    def set_utility_max_charging_current(self, value):
        pass

    @abstractmethod
    def set_bulk_charging_voltage(self, value):
        pass

    @abstractmethod
    def set_float_charging_voltage(self, value):
        pass
    
    @abstractmethod
    def set_battery_cutoff_voltage(self, value):
        pass

    @abstractmethod
    def set_input_voltage_range(self, value):
        pass


class DriverCommands(Enum):
    SET_OUTPUT_SOURCE="set_output_source"
    SET_CHARGER_SOURCE="set_charger_source"
    SET_BATTERY_RECHARGE_VOLTAGE="set_battery_recharge_voltage"
    SET_BATTERY_REDISCHARGE_VOLTAGE="set_battery_redischarge_voltage"
    SET_MAX_CHARGING_CURRENT="set_max_charging_current"
    SET_UTILITY_MAX_CHARGING_CURRENT="set_utility_max_charging_current"
    SET_BULK_CHARGING_VOLTAGE="set_bulk_charging_voltage"
    SET_FLOAT_CHARGING_VOLTAGE="set_float_charging_voltage"
    SET_BATTERY_CUTOFF_VOLTAGE="set_battery_cutoff_voltage"
    SET_INPUT_VOLTAGE_RANGE="set_input_voltage_range"