# region type_checking
from typing import List
# endregion /type_checking

from inverter.driver import IDriver, InverterData, InverterConfig
from inverter.usb_manager import Inverter, InverterHandler
import time


class Driver(IDriver):
    # static constant fields
    VENDOR_ID = 0x0665
    PRODUCT_ID = 0x5161
    IN_ENDPOINT = 0x81
    SERIAL_NUMBER_CMD = "QID"


    def __init__(self):
        self.handler = InverterHandler(
            Driver.VENDOR_ID,
            Driver.PRODUCT_ID,
            Driver.IN_ENDPOINT,
            Driver.SERIAL_NUMBER_CMD)


    @classmethod
    def is_inverter_compatible(cls, vendor_id: str, product_id: str) -> bool:
        return vendor_id == cls.VENDOR_ID and product_id == cls.PRODUCT_ID


    def load(self):
        inverters:List[Inverter] = self.handler.load_usb_devices()
        for inverter in inverters:
            inverter.set_driver(self)


    def get_inverters(self) -> List[Inverter]:
        return self.handler.inverters


    def get_inverter_data(self, serial_number:str) -> InverterData:
        qpigs = self._send_command(serial_number, "QPIGS").split(" ")
        qmod = self._send_command(serial_number, "QMOD")

        try:
            data = InverterData(
                time="%s"%int(time.time()),
                grid_voltage=qpigs[0],
                grid_frequency=qpigs[1],
                output_power=qpigs[5],
                pv_input_voltage=qpigs[13],
                pv_charging_power=qpigs[19],
                battery_charging_current=qpigs[9],
                battery_discharging_current=qpigs[15],
                power_source=qmod,
                battery_voltage=qpigs[8],
                load_percent=qpigs[6])
        except:
            raise RuntimeError("Inverter data is corrupt")
        return data


    def get_inverter_config(self, serial_number:str) -> InverterConfig:
        tries = 0

        while(tries < 3):
            try:
                qpiri = self._send_command(serial_number, "QPIRI").split(" ")

                config = InverterConfig(
                    time="%s"%int(time.time()),
                    output_source_priority=qpiri[16],
                    charger_source_priority=qpiri[17],
                    battery_recharge_voltage=qpiri[8],
                    battery_redischarge_voltage=qpiri[22],
                    max_charging_current=qpiri[14],
                    utility_max_charging_current=qpiri[13],
                    battery_bulk_voltage=qpiri[10],
                    battery_float_voltage=qpiri[11],
                    battery_cutoff_voltage=qpiri[9],
                    input_voltage_range=qpiri[15])

                return config
            except:
                tries += 1
        return None


    def _send_command(self, serial_number:str, cmd:str) -> str:
        write_success = self.handler.send_command(serial_number, cmd)
        # writing command failed
        if not write_success:
            return None

        # read response
        response = self.handler.read_response(serial_number)

        # reading failed
        if response is None:
            return None
        # invalid command
        if "NAK" in response:
            return None
        # response doesn't start with startbyte
        if not response.startswith('('):
            return None

        # success
        return response[1:]

# region set_config
    def set_output_source(self, value):
        pass

    def set_charger_source(self, value):
        pass

    def set_battery_recharge_voltage(self, value):
        pass

    def set_battery_redischarge_voltage(self, value):
        pass

    def set_max_charging_current(self, value):
        pass

    def set_utility_max_charging_current(self, value):
        pass

    def set_bulk_charging_voltage(self, value):
        pass

    def set_float_charging_voltage(self, value):
        pass
    
    def set_battery_cutoff_voltage(self, value):
        pass

    def set_input_voltage_range(self, value):
        pass
# endregion /set_config