# region type_checking
from typing import Generator, List
# endregion /type_checking

import usb.core, usb.util, usb.backend.libusb1
from usb.core import Device

from struct import pack
from crc16 import crc16xmodem
from inverter.driver import Inverter
import sys, time

from inverter import Inverter


class UsbInverter(Inverter):
    """Configured HID usb inverter
    """
    def __init__(self, usb_device:Device, serial_number:str):
        super().__init__(serial_number)
        self.usb_device = usb_device


class InverterHandler:
    """Send commands over USB to an HID inverter using control transfer,
    and receive data using IN-endpoint
    """


    def __init__(self, vendor_id:int, product_id:int, in_endpoint:int, serial_number_cmd:str):
        """
        Args:
            vendor_id (int): hexadecimal value of vendor-id
            product_id (int): hexadecimal value of product-id
            in_endpoint (int): hexadecimal address of the in-endpoint
            serial_number_cmd (str): command to retrieve the serial number from the usb device
        """
        self._vendor_id = vendor_id
        self._product_id = product_id
        self._in_endpoint = in_endpoint
        self._serial_number_cmd = serial_number_cmd

        self._inverters:List[UsbInverter] = []


    @property
    def inverters(self):
        return self._inverters


    def load_usb_devices(self) -> List[Inverter]:
        """load all usb devices with given vendor-id, product-id and in-endpoint

        Raises:
            RuntimeError: found an endpoint that is not specified
        """

        self._inverters.clear()

        # find all compatible connected inverters
        devices_gen:Generator[Device] = usb.core.find(
            find_all=True, 
            idVendor=self._vendor_id, 
            idProduct=self._product_id)

        # convert generator to list
        devices = [device for device in devices_gen]
        print("found %s device(s)" % len(devices))

        # load all the found devices
        for i, device in enumerate(devices):
            print("Loading device %s" % i)
            try:
                # get endpoint
                intf = device[0][(0, 0)]
                endpoint = intf.endpoints()[0]
                # make sure the endpoint is valid
                if endpoint.bEndpointAddress != self._in_endpoint:
                    raise RuntimeError("unexpected endpoint")

                # detach kernel to free usb device (only for linux)
                if sys.platform == "linux":
                    if device.is_kernel_driver_active(0):
                        device.detach_kernel_driver(0)

                # set device configuration
                # device.set_configuration()
                device.set_interface_altsetting(0,0)

                # flush read buffer
                self._send_command(device, self._serial_number_cmd)
                self._read_response(device)

                # get serial number
                self._send_command(device, self._serial_number_cmd)
                serial_number = self._read_response(device)[1:]

                # create UsbInverter
                inverter = UsbInverter(device, serial_number)
                self._inverters.append(inverter)

                print("Successfully loaded inverter %s" % i)
            # except RuntimeError:
            #     print("Failed to load device %s: Unexpected endpoint" % i)
            except Exception as e:
                print(e)
                print("Failed to load device %s" % i)
        return self._inverters

    def get_usb_device(self, serial_number:str) -> Device:
        """get the usb device with the given serial number

        Args:
            serial_number (str): serial number

        Returns:
            Device: USB device with given serial number
        """
        # find inverter
        for inverter in self._inverters:
            if inverter.serial_number == serial_number:
                return inverter.usb_device
        # inverter not found
        return None


    def _increment_forbidden_bytes(self, b:bytearray, forbidden_bytes:list) -> bytearray:
        for i, byte in enumerate(b):
            if byte in forbidden_bytes:
                b[i] = byte+1
        return b

    
    def _send_command(self, device:Device, cmd:str) -> bool:
        """encode a command and send it to usb device using control transfer with 
        standard HID parameters, encoding is done using crc16 checksum

        Args:
            device (Device): usb device (usb.core.Device)
            cmd (str): unencoded command
        """
        cmd = cmd.encode('utf-8')
        crc_unchecked = pack('>H',crc16xmodem(cmd))
        # fix forbidden bytes \r, \n, ( to the byte + 1
        crc = self._increment_forbidden_bytes(bytearray(crc_unchecked), [0x0D, 0x0A, 0x28])
        cmd = cmd + crc + b'\r'

        try:
            device.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd[:8])
            # commands can be longer than 8 bytes
            if len(cmd) > 8:
                device.ctrl_transfer(0x21, 0x9, 0x200, 0, cmd[8:])
        except:
            return False
        return True


    def send_command(self, serial_number:str, cmd:str) -> bool:
        """encode a command and send it to inverter with given serial number using
        control transfer with standard HID parameters, encoding is done using crc16 checksum

        Args:
            serial_number (str): serial number
            cmd (str): unencoded command
        """
        # get usb device from serial number
        device = self.get_usb_device(serial_number)
        return self._send_command(device, cmd)
        

    def _read_response(self, device:Device, timeout:int=100) -> str:
        """read data from specified in-endpoint from usb device

        Args:
            device (Device): usb device (usb.core.Device)
            timeout (int, optional): read timeout in ms

        Returns:
            str: data that was read
            None: inverter returns empty
        """
        res=""
        i=0
        while i<20 and '\r' not in res:
            try:
                res+="".join([chr(i) for i in device.read(self._in_endpoint, 8, timeout)])
            except:
                pass
            i+=1
        return res


    def read_response(self, serial_number:str, timeout:int=100) -> str:
        """read data from specified in-endpoint from inverter with given serial number

        Args:
            serial_number (str): serial number
            timeout (int, optional): read timeout in ms

        Returns:
            str: data that was read
        """
        # get usb device from serial number
        device = self.get_usb_device(serial_number)
        return self._read_response(device, timeout)