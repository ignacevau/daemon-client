from typing import List

from inverter.driver import driver_hb
from inverter import Inverter


class InverterHandler:
    inverters:List[Inverter] = []


    def __init__(self):
        # dont allow instances of this class
        raise RuntimeError("Instances of this class are not allowed")


    @classmethod
    def reload_inverters(cls) -> None:
        cls.inverters.clear()

        # load drivers
        hb_driver = driver_hb.Driver()
        hb_driver.load()
        cls.inverters.extend(hb_driver.get_inverters())


    @classmethod
    def get_inverter(cls, serial_number:str, product_id:int, vendor_id:int) -> Inverter:
        for inverter in cls.inverters:
            if inverter.compare_id(serial_number, product_id, vendor_id):
                return inverter
        return None


    @classmethod
    def get_all_inverters(cls) -> List[Inverter]:
        return cls.inverters