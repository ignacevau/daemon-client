class Inverter:
    """Generic Inverter object with serial number
    """
    def __init__(self, serial_number:str, product_id:int, vendor_id:int):
        self._serial_number = serial_number
        self._product_id = product_id
        self._vendor_id = vendor_id
        self._driver = None

    def __eq__(self, other):
        if isinstance(other, Inverter):
            return other.serial_number == self.serial_number
        return False


    def set_driver(self, driver) -> None:
        self._driver = driver

    def get_driver(self):
        return self._driver

    def compare_id(self, serial_number:str, product_id:int, vendor_id:int) -> bool:
        return (self._serial_number, self._product_id, self._vendor_id) == (serial_number, product_id, vendor_id)