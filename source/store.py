# SERVER URLS
# SERVER_BASE_URL = "https://d12zqkyuq0ro7o.cloudfront.net"
SERVER_BASE_URL = "http://localhost:5000"

SERVER_COMMAND_URL = "%s/inverter/command-buffer"%SERVER_BASE_URL
SERVER_INVERTER_DATA_URL = "%s/inverter/upload-data"%SERVER_BASE_URL
SERVER_INVERTER_CONFIG_URL = "%s/inverter/upload-config"%SERVER_BASE_URL


# CONSTANTS
INVERTER_RELOAD_DELAY = 5   # interval (seconds) between reloading all inverters
HELPER_SEND_DELAY = 1       # interval (seconds) between sending status to daemon-helper
SERVER_SEND_DELAY = 60      # interval (seconds) between auto-sending data to server
SERVER_COMMAND_DELAY = 1    # interval (seconds) between checking the server command buffer

LOOP_INTERVAL = 0.5         # interval (seconds) between each main loop iteration

SERVER_TIMEOUT = 3          # timeout for connecting to server