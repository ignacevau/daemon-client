import time, sys

from inverter.inverter_handler import InverterHandler
from inverter.driver import InverterData, InverterConfig, IDriver

from connection import Client, StatusCodes
from logger import Logger
import store
from server import Server


class TimeFrame:
    def __init__(self, interval:float, start_delay=True):
        self.interval = interval
        self.timeframe = time.time()

        # no start delay
        if not start_delay:
            self.timeframe = 0


    def check(self) -> bool:
        if time.time() - self.timeframe > self.interval:
            self.timeframe = time.time()
            return True
        return False


class DaemonClient:
    def __init__(self):
        self.client = None
        self.connection = None
        self.running = False
        
        # timeframes (all actions have different delays)
        self.reload_tf:TimeFrame = TimeFrame(store.INVERTER_RELOAD_DELAY)
        self.status_tf:TimeFrame = TimeFrame(store.HELPER_SEND_DELAY, start_delay=False)
        self.server_command_tf:TimeFrame = TimeFrame(store.SERVER_COMMAND_DELAY)
        self.server_send_tf:TimeFrame = TimeFrame(store.SERVER_SEND_DELAY, start_delay=False)


    def run(self):
        self.client = Client(('localhost', 6000), 2)

        # connect to helper
        try:
            self.connection = self.client.connect()
            Logger.log("Connected to helper")
        except Exception as e:
            # couldn't connect
            Logger.error("Couldn't connect: %s" % e)
            sys.exit()

        # start the main loop
        self.last_time_frame = time.time()
        self.running = True
        self.main_loop()


    def main_loop(self):
        while self.running:
            # send status to helper
            if self.status_tf.check():
                self.send_helper_status()

            # reload inverters
            if self.reload_tf.check():
                self.reload_inverters()

            # send data to server
            if self.server_send_tf.check():
                self.send_server_data()
                self.send_server_config()

            # get commands from server
            if self.server_command_tf.check():
                pass
            

            # loop runs every x seconds
            time.sleep(store.LOOP_INTERVAL)


    def send_helper_status(self):
        try:
            self.connection.send(StatusCodes.HEALTHY)
            Logger.log("Status sent to helper")
        # timeout
        except TimeoutError:
            Logger.log("Helper didn't respond, quitting...")
            self.stop()
        # sending message failed
        except Exception as e:
            Logger.error("Something went wrong while sending data to helper: %s" % e)
            self.stop()


    def send_server_data(self):
        Logger.log("Sending inverter data to server")

        for inverter in InverterHandler.get_all_inverters():
            try:
                driver:IDriver = inverter.get_driver()
                data:InverterData = driver.get_inverter_data(inverter.get_serial_number())
            # couldn't fetch data -> reload the inverters
            except:
                Logger.error("Fetching inverter data failed")
                self.reload_inverters()
                return

            if data.is_valid():
                success = Server.send_inverter_data(data)

                if not success:
                    Logger.warn("Couldn't send inverter data to server; Caching data...")
                    # TODO cache data


    def send_server_config(self):
        Logger.log("Sending inverter config to server")

        for inverter in InverterHandler.get_all_inverters():
            try:
                driver:IDriver = inverter.get_driver()
                config:InverterConfig = driver.get_inverter_config(inverter.get_serial_number())
            # couldn't fetch config -> reload the inverters
            except:
                Logger.error("Fetching inverter config failed")
                self.reload_inverters()
                return

            if config.is_valid():
                success = Server.send_inverter_config(config)

                if not success:
                    Logger.warn("Couldn't send inverter config to server; Caching config...")
                    # TODO cache config


    def reload_inverters(self):
        Logger.log("Reloading inverters")
        InverterHandler.reload_inverters()


    def stop(self):
        # close connection
        if self.connection is not None:
            self.connection.close()
        # close client
        if self.client is not None:
            self.client.close()
            
        self.running = False

        # close the program, helper will restart it
        sys.exit()


if __name__ == "__main__":
    client = DaemonClient()
    client.run()