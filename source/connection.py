from multiprocessing.connection import Connection, util
import socket, struct, os
from enum import Enum


class StatusCodes(Enum):
    CRITICAL = 0,
    HEALTHY = 1


class Client:
    def __init__(self, address, send_timeout:int):
        self._address = address
        self._send_timeout = send_timeout
        self._connection = None
        self._socket = None

    
    def connect(self) -> Connection:
        with socket.socket(socket.AF_INET) as s:
            self._socket = s
            s.setblocking(True)
            s.connect(self._address)

            # set send timeout
            seconds = self._send_timeout*1000
            # timeval scale may differ on other platforms
            timeval = struct.pack("@L", seconds)

            # set the SO_SNDTIMEO (send timeout) option
            s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDTIMEO, timeval)

            # create the connection
            self._connection = Connection(s.detach())
            return self._connection


    def close(self):
        try:
            self._socket.close()
        except:
            pass


class Listener(object):
    def __init__(self, address, accept_timeout:int, receive_timeout:int):
        family = "AF_INET"
        backlog = 1

        self._socket = socket.socket(getattr(socket, family))
        try:
            # SO_REUSEADDR has different semantics on Windows (issue #2550).
            if os.name == 'posix':
                self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            
            # set receive timeout
            seconds = receive_timeout*1000
            # timeval scale may differ on other platforms
            timeval = struct.pack("@L", seconds)
            # set the SO_RCVTIMEO (receive timeout) option
            self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVTIMEO, timeval)

            self._socket.setblocking(True)
            self._socket.bind(address)
            self._socket.listen(backlog)
            self._address = self._socket.getsockname()

            # set accept timeout
            self._socket.settimeout(accept_timeout)
        except OSError:
            self._socket.close()
            raise
        self._family = family
        self._last_accepted = None

    def accept(self) -> Connection:
        s, self._last_accepted = self._socket.accept()
        s.setblocking(True)
        return Connection(s.detach())

    def close(self):
        try:
            self._socket.close()
        except:
            pass