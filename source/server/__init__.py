from inverter.driver import InverterData, InverterConfig
import requests
import store
from logger import Logger


class Server:
    def __init__(self):
        raise RuntimeError("Not allowed to create instances of this class")


    @classmethod
    def send_inverter_data(cls, data:InverterData, serial:str, vendor_id:int, product_id:int) -> bool:
        packet = {
            "serial_number": serial,
            "vendor_id": vendor_id,
            "product_id": product_id
        }

        packet.update(data.to_dict())

        # send the data
        try:
            response = requests.post(store.SERVER_INVERTER_DATA_URL, json=packet, timeout=store.SERVER_TIMEOUT)
            # check if data was successfully sent
            if "data" in response.json():
                return True
            # something went wrong on the server
            return False
        # couldn't send data to server
        except:
            return False
    
    
    @classmethod
    def send_inverter_config(cls, config:InverterConfig, serial:str, vendor_id:int, product_id:int):
        packet = {
            "serial_number": serial,
            "vendor_id": vendor_id,
            "product_id": product_id
        }

        packet.update(config.to_dict())

        try:
            response = requests.post(store.SERVER_INVERTER_CONFIG_URL, json=packet)
            # check if config was successfully sent
            if "data" in response.json():
                return True
            # something went wrong on the server
            return False
        # couldn't send config to server
        except:
            return False